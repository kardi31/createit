<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\WeatherRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WeatherRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method WeatherRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method WeatherRequest[]    findAll()
 * @method WeatherRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeatherRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WeatherRequest::class);
    }

    /**
     * @param WeatherRequest $weatherRequest
     * @return void
     * @throws ORMException
     */
    public function save(WeatherRequest $weatherRequest): void
    {
        $em = $this->getEntityManager();
        $em->persist($weatherRequest);
        $em->flush($weatherRequest);
    }
}
