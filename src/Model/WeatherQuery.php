<?php
declare(strict_types=1);

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class WeatherQuery
{
    /**
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 3,
     *      allowEmptyString = false
     * )
     * @var string
     */
    private string $city;

    /**
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 3,
     *      allowEmptyString = false
     * )
     * @var string
     */
    private string $country;

    /**
     * @param string $city
     * @param string $country
     */
    public function __construct(string $city, string $country)
    {
        $this->city = $city;
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }
}
