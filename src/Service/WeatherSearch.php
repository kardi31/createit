<?php
declare(strict_types=1);
/**
 * Class WeatherSearch
 *
 * @author Tomasz Kardas <kardi31@tlen.pl>
 */

namespace App\Service;

use App\Client\WeatherClientInterface;
use App\Exception\InvalidRequestException;
use App\Exception\InvalidResponseException;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class WeatherSearch
{
    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;

    /**
     * @var WeatherClientInterface[]
     */
    private array $clients = [];

    /**
     * @param LoggerInterface|null $logger
     */
    public function __construct(?LoggerInterface $logger = null)
    {
        $this->logger = $logger ?? new NullLogger();
    }

    /**
     * Returns average temperature for city and country based on data from all registered clients
     * @param string $city
     * @param string $country
     * @return int|null
     */
    public function findTemperatureByCityAndCountry(string $city, string $country): ?int
    {
        $temperatures = [];
        foreach ($this->getClients() as $client) {
            try {
                $temperatures[] = $client->getTemperatureByCityAndCountry($city, $country);
            } catch (InvalidRequestException|InvalidResponseException $e) {
                $this->logger->error(
                    sprintf(
                        'Failed to obtain temperature from %s for city %s and country %s',
                        get_class($client),
                        $city,
                        $country
                    )
                );
            }
        }

        // remove empty elements
        $temperatures = array_values($temperatures);

        if (empty($temperatures)) {
            return null;
        }

        return (int) ceil(array_sum($temperatures) / count($temperatures));
    }

    /**
     * @param WeatherClientInterface $client
     * @return void
     */
    public function addClient(WeatherClientInterface $client): void
    {
        $this->clients[] = $client;
    }

    /**
     * @return WeatherClientInterface[]
     */
    public function getClients(): array
    {
        return $this->clients;
    }

}
