<?php
declare(strict_types=1);

namespace App\Controller;

use App\Command\AddWeatherRequestCommand;
use App\Entity\WeatherRequest;
use App\Form\WeatherQueryType;
use App\Model\WeatherQuery;
use App\Service\WeatherSearch;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class WeatherController extends AbstractController
{
    /**
     * @Route("/", name="weather_search", methods={"GET","POST"})
     * @param Request $request
     * @param WeatherSearch $weatherSearchService
     * @param MessageBusInterface $messageBus
     * @return Response
     */
    public function search(
        Request $request,
        WeatherSearch $weatherSearchService,
        MessageBusInterface $messageBus
    ): Response {
        $weatherQuery = new WeatherQuery('', '');
        $form = $this->createForm(WeatherQueryType::class, $weatherQuery);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var WeatherRequest $data */
            $data = $form->getData();
            $temperature = $weatherSearchService->findTemperatureByCityAndCountry($data->getCity(), $data->getCountry());
            if ($temperature) {
                $messageBus->dispatch(
                    new AddWeatherRequestCommand($temperature, $data->getCity(), $data->getCountry())
                );
            }
        }

        return $this->render('weather/search.html.twig', [
            'temperature' => $temperature ?? null,
            'weatherQuery' => $weatherQuery,
            'form' => $form->createView(),
        ]);
    }
}
