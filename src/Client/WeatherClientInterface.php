<?php
declare(strict_types=1);
/**
 * Class WeatherClientInterface
 *
 * @author Tomasz Kardas <kardi31@tlen.pl>
 */

namespace App\Client;

use App\Exception\InvalidRequestException;
use App\Exception\InvalidResponseException;

interface WeatherClientInterface
{
    /**
     * @param string $city
     * @param string $country
     * @return float|null
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function getTemperatureByCityAndCountry(string $city, string $country): ?float;
}
