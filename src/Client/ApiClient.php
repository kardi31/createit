<?php
declare(strict_types=1);
/**
 * Class ApiClient
 *
 * @author Tomasz Kardas <kardi31@tlen.pl>
 */

namespace App\Client;

use App\Exception\InvalidRequestException;
use App\Exception\InvalidResponseException;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

abstract class ApiClient implements ApiClientInterface
{
    public const METHOD_GET = 'GET';

    /**
     * @var LoggerInterface|null
     */
    protected ?LoggerInterface $logger;

    /**
     * @var string
     */
    protected string $apiUrl;

    /**
     * @param string $apiUrl
     * @param LoggerInterface|null $logger
     */
    public function __construct(string $apiUrl, ?LoggerInterface $logger = null)
    {
        $this->logger = $logger ?? new NullLogger();
        $this->apiUrl = $apiUrl;
    }

    /**
     * @param string $url
     * @param string $method
     * @param array $parameters
     * @return ResponseInterface|null
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    protected function doRequest(string $url, string $method, array $parameters = []): ResponseInterface
    {
        $client = $this->getClient();
        try {
            $result = $client->request($method, $url, !empty($parameters) ? ['query' => $parameters] : []);

            /**
             * Some API endpoints return blank response when data for request doesn't exist. This is corrected here, as we want to have some indication when the data for request exists or not.
             */
            if (empty((string) $result->getBody())) {
                throw new InvalidResponseException('API request was invalid. Response is blank');
            }

            return $result;
        } catch (GuzzleException $e) {
            $this->logger->error($e->getMessage());
            throw new InvalidRequestException(sprintf('Request failed with error message %s', $e->getMessage()));
        }
    }
    /**
     * @return ClientInterface
     */
    protected function getClient(): ClientInterface
    {
        return new Client([
            'base_uri' => $this->apiUrl,
            'timeout'  => 2.0,
        ]);
    }
}
