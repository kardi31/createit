<?php
declare(strict_types=1);
/**
 * Class ApiClient
 *
 * @author Tomasz Kardas <kardi31@tlen.pl>
 */

namespace App\Client;

use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class OpenWeatherClient extends ApiClient implements WeatherClientInterface
{
    use CachedClientDataTrait;

    private const CLIENT_KEY = 'open_weather';
    private const METRIC_UNITS = 'metric';

    /**
     * @var string
     */
    protected string $apiKey;

    /**
     * @var LoggerInterface|null
     */
    protected ?LoggerInterface $logger;

    /**
     * @param string $apiUrl
     * @param string $apiKey
     * @param int $cacheLifetime
     * @param LoggerInterface|null $logger
     */
    public function __construct(string $apiUrl, string $apiKey, int $cacheLifetime, ?LoggerInterface $logger = null)
    {
        $this->apiKey = $apiKey;
        $this->cache = new FilesystemAdapter();
        $this->cacheLifetime = $cacheLifetime;
        parent::__construct($apiUrl, $logger);
    }

    /**
     * @inheritDoc
     */
    public function getTemperatureByCityAndCountry(string $city, string $country): ?float
    {
        $cachedData = $this->getCachedClientData($city, $country);
        if ($cachedData) {
            return $cachedData;
        }

        $parameters = $this->buildRequestParameters($city, $country);
        $response = $this->doRequest('', self::METHOD_GET, $parameters);
        $content = json_decode((string) $response->getBody(), true);

        if (!isset($content['main']['temp'])) {
            return null;
        }

        $temperature = (float) $content['main']['temp'];

        $this->setCachedClientData($temperature, $city, $country);

        return $temperature;
    }

    /**
     * @param string $city
     * @param string $country
     * @return array
     */
    private function buildRequestParameters(string $city, string $country): array
    {
        return [
            'q' => sprintf('%s,%s', $city, $country),
            'appid' => $this->apiKey,
            'units' => self::METRIC_UNITS
        ];
    }
}
