<?php
declare(strict_types=1);
/**
 * Class CachedClientDataTrait
 *
 * @package App\Client
 * @author Tomasz Kardas <kardi31@tlen.pl>
 */

namespace App\Client;

use Symfony\Component\Cache\CacheItem;
use Symfony\Contracts\Cache\CacheInterface;

trait CachedClientDataTrait
{
    /**
     * @var CacheInterface
     */
    private CacheInterface $cache;

    /**
     * @var int
     */
    protected int $cacheLifetime;

    /**
     * @param $city
     * @param $country
     * @return float|null
     */
    public function getCachedClientData($city, $country): ?float
    {
        $cacheKey = $this->getCacheKey($city, $country);
        if ($this->cache->hasItem($cacheKey)) {
            return (float) $this->cache->getItem($cacheKey)->get();
        }

        return null;
    }

    /**
     * @param float $temperature
     * @param string $city
     * @param string $country
     * @return void
     */
    public function setCachedClientData(float $temperature, string $city, string $country): void
    {
        $cacheKey = $this->getCacheKey($city, $country);

        /** @var CacheItem $item */
        $item = $this->cache->getItem($cacheKey);
        $item->expiresAfter($this->cacheLifetime);
        $item->set($temperature);
        $this->cache->save($item);
    }

    /**
     * @param string $city
     * @param string $country
     * @return string
     */
    public function getCacheKey(string $city, string $country): string
    {
        return sprintf(
            '%s_%s_%s_%s',
            self::CLIENT_KEY,
            urlencode($city),
            urlencode($country),
            date('ymd')
        );
    }
}
