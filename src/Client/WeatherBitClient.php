<?php
declare(strict_types=1);
/**
 * Class ApiClient
 *
 * @author Tomasz Kardas <kardi31@tlen.pl>
 */

namespace App\Client;

use App\Service\CountryCode;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class WeatherBitClient extends ApiClient implements WeatherClientInterface
{
    use CachedClientDataTrait;

    private const CLIENT_KEY = 'weather_bit';
    private const METRIC_UNITS = 'M';

    /**
     * @var string
     */
    protected string $apiKey;

    /**
     * @var LoggerInterface|null
     */
    protected ?LoggerInterface $logger;

    /**
     * @var CountryCode
     */
    protected CountryCode $countryCode;

    /**
     * @param string $apiUrl
     * @param string $apiKey
     * @param int $cacheLifetime
     * @param CountryCode $countryCode
     * @param LoggerInterface|null $logger
     */
    public function __construct(
        string $apiUrl,
        string $apiKey,
        int $cacheLifetime,
        CountryCode $countryCode,
        ?LoggerInterface $logger = null
    ) {
        $this->apiKey = $apiKey;
        $this->cache = new FilesystemAdapter();
        $this->cacheLifetime = $cacheLifetime;
        parent::__construct($apiUrl, $logger);
        $this->countryCode = $countryCode;
    }

    /**
     * @inheritDoc
     */
    public function getTemperatureByCityAndCountry(string $city, string $country): ?float
    {
        $cachedData = $this->getCachedClientData($city, $country);
        if ($cachedData) {
            return $cachedData;
        }

        // weatherbit API requires country code to be passed, instead of country
        $countryCode = $this->countryCode->getCountryCode($country);
        if (!$countryCode) {
            return null;
        }

        $parameters = $this->buildRequestParameters($city, $countryCode);
        $response = $this->doRequest('', self::METHOD_GET, $parameters);
        $content = json_decode((string) $response->getBody(), true);

        if (!isset($content['data'][0]['temp'])) {
            return null;
        }

        $temperature = (float) ($content['data'][0]['temp']);

        $this->setCachedClientData($temperature, $city, $country);

        return $temperature;
    }

    /**
     * @param string $city
     * @param string $country
     * @return array
     */
    private function buildRequestParameters(string $city, string $country): array
    {
        return [
            'key' => $this->apiKey,
            'units' => self::METRIC_UNITS,
            'country' => $country,
            'city' => $city
        ];
    }
}
