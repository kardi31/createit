<?php
declare(strict_types=1);
/**
 * Class WeatherCompiler
 *
 * @package App\DependencyInjection\Compiler
 * @author Tomasz Kardas <kardi31@tlen.pl>
 */

namespace App\DependencyInjection\Compiler;

use App\Service\WeatherSearch;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class WeatherClientPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // always first check if the primary service is defined
        if (!$container->has(WeatherSearch::class)) {
            return;
        }

        $definition = $container->findDefinition(WeatherSearch::class);

        $taggedServices = $container->findTaggedServiceIds('app.weather_client');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addClient', [new Reference($id)]);
        }
    }
}

