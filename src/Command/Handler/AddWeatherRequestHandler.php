<?php
declare(strict_types=1);

namespace App\Command\Handler;

use App\Command\AddWeatherRequestCommand;
use App\Entity\WeatherRequest;
use App\Repository\WeatherRequestRepository;
use Doctrine\ORM\ORMException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AddWeatherRequestHandler implements MessageHandlerInterface
{
    /**
     * @var WeatherRequestRepository
     */
    protected WeatherRequestRepository $weatherRequestRepository;

    /**
     * @param WeatherRequestRepository $weatherRequestRepository
     */
    public function __construct(WeatherRequestRepository $weatherRequestRepository)
    {
        $this->weatherRequestRepository = $weatherRequestRepository;
    }

    /**
     * @param AddWeatherRequestCommand $command
     * @return void
     * @throws ORMException
     */
    public function __invoke(AddWeatherRequestCommand $command)
    {
        $weatherRequest = new WeatherRequest($command->getCity(), $command->getCountry(), $command->getTemperature());
        $this->weatherRequestRepository->save($weatherRequest);
    }
}
