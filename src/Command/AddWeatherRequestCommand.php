<?php
declare(strict_types=1);
/**
 * Class AddWeatherQueryCommand
 *
 * @package App\Command
 * @author Tomasz Kardas <kardi31@tlen.pl>
 */

namespace App\Command;

class AddWeatherRequestCommand
{
    /**
     * @var int
     */
    protected int $temperature;

    /**
     * @var string
     */
    protected string $city;

    /**
     * @var string
     */
    protected string $country;

    /**
     * @param int $temperature
     * @param string $city
     * @param string $country
     */
    public function __construct(int $temperature, string $city, string $country)
    {
        $this->temperature = $temperature;
        $this->city = $city;
        $this->country = $country;
    }

    /**
     * @return int
     */
    public function getTemperature(): int
    {
        return $this->temperature;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }
}
