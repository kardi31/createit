<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\WeatherRequestRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WeatherRequestRepository::class)
 */
class WeatherRequest
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private string $city;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private string $country;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private int $temperature;

    /**
     * @param string $city
     * @param string $country
     * @param int $temperature
     */
    public function __construct(string $city, string $country, int $temperature)
    {
        $this->city = $city;
        $this->country = $country;
        $this->temperature = $temperature;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @return int
     */
    public function getTemperature(): int
    {
        return $this->temperature;
    }
}
