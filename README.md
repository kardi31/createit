# About

Application displaying current temperature for city & country

## Installation

1. Create a database and put its credentials in `.env`
2. Run `bin/console doctrine:mi:mi`
3. If you are on development environment, run `symfony server:start`

## Usage

1. Go to homepage
2. Put city and country. Country name should be in english
3. Click search

